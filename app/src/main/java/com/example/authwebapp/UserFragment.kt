package com.example.authwebapp

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.Navigation
import androidx.navigation.fragment.NavHostFragment.Companion.findNavController
import kotlinx.coroutines.flow.collectLatest

class UserFragment : Fragment() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view : View = inflater.inflate(R.layout.fragment_user, container, false)


        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        val userViewModel : UserViewModel by activityViewModels()

        val logOutButton : Button = view.findViewById(R.id.log_out_button)
        val usernameTextView : TextView = view.findViewById(R.id.username)
        val emailTextView : TextView =  view.findViewById(R.id.email)

        lifecycleScope.launchWhenStarted{
            val token = userViewModel.token.value
            if (token != null) {
                userViewModel.authUsersToken()
            }
            userViewModel.loginState.collectLatest{ state ->
                usernameTextView.text = state.username
                emailTextView.text = state.email


            }
        }

        logOutButton.setOnClickListener{
            userViewModel.onEvent(LoginFormEvent.LoggedOut)
            Navigation.findNavController(view).navigate(R.id.loginFragment)
        }

        super.onViewCreated(view, savedInstanceState)
    }

    companion object {
        @JvmStatic
        fun newInstance() = UserFragment()
    }
}