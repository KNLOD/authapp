package com.example.authwebapp

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch

class SharedWebViewModel() : ViewModel() {

    private var _urlLink= MutableLiveData<String>("https://www.google.com")
    val urlLink : LiveData<String>
    get() = _urlLink

    // Used to share logic WebView.canGoBack() in WebFragment to MainActivity
    private var _webViewCanGoBack = MutableLiveData<Boolean>(false)
    val webViewCanGoBack : LiveData<Boolean>
    get() = _webViewCanGoBack

    // Used to observe MainActivity's OnBackPressed() in WebView fragment
    // if @goBack changed to true -> webView.goBack() is called
    private var _goBack = MutableLiveData<Boolean>(false)
    val goBack: LiveData<Boolean>
    get() = _goBack



    /**
     * Saves last opened Link
     * to be saved later in sharedPreferences
     */
    fun setLink(link : String){
        _urlLink.value = link

    }

    /**
     * Saves state of WebView's canGoBack() to be used in MainActivity
     * in OnBackPressed()
      */
    fun setGoBackState(canGoBack : Boolean) {
        _webViewCanGoBack.value = canGoBack
    }


    /**
     * Changes goBack value in ViewModel
     * so that when it is changed to True
     * WebView.goBack() is called
     */
    fun makeWebViewGoBack(value : Boolean){
        _goBack.value = value
    }



}