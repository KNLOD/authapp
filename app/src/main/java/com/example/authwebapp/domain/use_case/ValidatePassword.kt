package com.example.authwebapp.domain.use_case

class ValidatePassword {
    fun execute(password : String) : ValidationResult{
        if (password.isBlank()){
            return ValidationResult(
                successful = false,
                errorMessage = "The password should not be blank"
            )
        }
        val containsDigitsAndLetters = password.any{it.isDigit()} && password.any{it.isLetter()}
        if (!containsDigitsAndLetters){
            return ValidationResult(
                successful = false,
                errorMessage = "The password should contain at least one digit and letter"
            )
        }
        return ValidationResult(
            successful = true
        )
    }
}