package com.example.authwebapp.domain.use_case

class ValidateRepeatedPassword {
    fun execute(password : String, repeatedPassword : String) : ValidationResult{
        if (password != repeatedPassword){
            return ValidationResult(
                successful = false,
                errorMessage = "Passwords should be the same"
            )
        }
        return ValidationResult(
            successful = true
        )
    }
}