package com.example.authwebapp.domain.use_case

import android.util.Patterns

class ValidateEmail {

    fun execute(email : String) : ValidationResult{
        if (email.isBlank()){
            return ValidationResult(
                successful = false,
                errorMessage = "E-Mail should not be blank"
            )
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            return ValidationResult(
                successful = false,
                errorMessage = "The E-Mail is not valid"
            )
        }

        return ValidationResult(
            successful = true
        )


    }
}