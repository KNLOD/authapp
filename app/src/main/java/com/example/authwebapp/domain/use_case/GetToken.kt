package com.example.authwebapp.domain.use_case

import java.security.MessageDigest

class GetToken {

    fun sha256(str : String) : String{
        val byteArray = MessageDigest.getInstance("SHA-256")
            .digest(str.toByteArray(Charsets.UTF_8))

        val hashString = byteArray.joinToString(separator = ""){byte ->
            "%02x".format(byte)
        }

        return hashString
    }
}