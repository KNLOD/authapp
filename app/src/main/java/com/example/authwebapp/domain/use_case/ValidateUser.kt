package com.example.authwebapp.domain.use_case

import com.example.authwebapp.model.Users

class ValidateUser {
    fun execute(users : Users, username : String, token : String) : ValidationResult{

        for (user in users.list){
            if (user.username == username && user.token == token){
                return ValidationResult(
                    successful = true
                )
            }
        }
        return ValidationResult(
            successful = false,
            errorMessage = "Username or Password is wrong"
        )

    }
}