package com.example.authwebapp.domain.use_case

class ValidateUsername {
    fun execute(username :  String) : ValidationResult{
        if (username.isBlank()){
            return ValidationResult(
                successful = false,
                errorMessage = "Username should not be blank"
            )
        }
        return ValidationResult(
            successful = true
        )
    }
}