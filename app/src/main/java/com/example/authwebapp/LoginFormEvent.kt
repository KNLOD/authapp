package com.example.authwebapp


sealed class LoginFormEvent {
    data class Submit(val username : String, val password : String) : LoginFormEvent()
    data class AuthenticateUsersToken(val token : String) : LoginFormEvent()
    object LoggedOut : LoginFormEvent()
}