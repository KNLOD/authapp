package com.example.authwebapp

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.core.widget.addTextChangedListener
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.Navigation
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import kotlinx.coroutines.flow.collectLatest

class RegistrationFragment : Fragment() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view : View = inflater.inflate(R.layout.fragment_registration, container, false)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        val signUpButton : Button = view.findViewById(R.id.sign_up)

        val usernameEdit : TextInputEditText = view.findViewById(R.id.username)
        val usernameField : TextInputLayout = view.findViewById(R.id.username_registration_field)

        val emailEdit : TextInputEditText = view.findViewById(R.id.email)
        val emailField : TextInputLayout = view.findViewById(R.id.email_registration_field)

        val passwordEdit : TextInputEditText = view.findViewById(R.id.password)
        val passwordField : TextInputLayout = view.findViewById(R.id.password_registration_field)

        val repeatedPasswordEdit : TextInputEditText = view.findViewById(R.id.confirm_password)
        val repeatedPasswordField: TextInputLayout = view.findViewById(R.id.confirm_password_field)


        val userViewModel : UserViewModel by activityViewModels()

        emailEdit.doAfterTextChanged{ email ->
            userViewModel.onEvent(RegistrationFormEvent
                .EmailChanged(email.toString()))

            lifecycleScope.launchWhenStarted{
                userViewModel.state.collectLatest{ state ->
                    emailField.isErrorEnabled = state.emailError != null
                    emailField.error = state.emailError
                }

            }
        }
        usernameEdit.doAfterTextChanged{ username ->
            userViewModel.onEvent(RegistrationFormEvent
                .UsernameChanged(username.toString())
            )
            lifecycleScope.launchWhenStarted{
                userViewModel.state.collectLatest{ state ->
                    usernameField.isErrorEnabled = state.emailError != null
                    usernameField.error = state.usernameError
                }
            }

        }
        passwordEdit.doAfterTextChanged{ password ->
            userViewModel.onEvent(RegistrationFormEvent
                .PasswordChanged(password.toString()))
            lifecycleScope.launchWhenStarted{
                userViewModel.state.collectLatest{ state ->
                    passwordField.isErrorEnabled = state.passwordError != null
                    passwordField.error = state.passwordError
                }
            }
        }

        repeatedPasswordEdit.doAfterTextChanged{ repeatedPassword ->
            userViewModel.onEvent(RegistrationFormEvent
                .RepeatedPasswordChanged(
                    repeatedPassword.toString()
                ))
            lifecycleScope.launchWhenStarted{
                userViewModel.state.collectLatest{ state ->
                    repeatedPasswordField.isErrorEnabled = state.repeatedPasswordError != null
                    repeatedPasswordField.error = state.repeatedPasswordError
                }
            }

        }

        signUpButton.setOnClickListener{
            userViewModel.onEvent(RegistrationFormEvent.Submit)
            lifecycleScope.launchWhenStarted{
                userViewModel.state.collectLatest{ state ->

                    if (state.isAllDataValid){
                        userViewModel.signUp()
                        Toast.makeText(context, "Registration is done!", Toast.LENGTH_SHORT ).show()
                        Navigation.findNavController(it).navigate(R.id.action_registrationFragment_to_loginFragment)

                    }
                }
            }
            /*
            userViewModel.signUp(
                emailEdit.text.toString(),
                userNameEdit.text.toString(),
                passwordEdit.text.toString(),
                repeatedPasswordEdit.text.toString()
            )
            */
            //Navigation.findNavController(it).navigate(R.id.action_registrationFragment_to_loginFragment)



        }
        super.onViewCreated(view, savedInstanceState)
    }

}