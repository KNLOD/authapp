package com.example.authwebapp

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.authwebapp.data.UsersDataSource
import com.example.authwebapp.domain.use_case.*
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import java.io.File

class UserViewModel() : ViewModel() {
    val SALT = "VibeLab"

    private var _username = MutableLiveData<String>("")
    val username : LiveData<String>
        get() = _username

    private var _password = MutableLiveData<String>("")
    val password : LiveData<String>
        get() = _password

    private var _email = MutableLiveData<String>("")
    val email : LiveData<String>
        get() = _email

    private var _token = MutableLiveData<String>("")
    val token : LiveData<String>
        get() = _token

    private var _state = MutableStateFlow<RegistrationFormState>(RegistrationFormState())
    val state = _state.asStateFlow()

    private var _loginState = MutableStateFlow<LoginFormState>(LoginFormState())
    val loginState = _loginState.asStateFlow()

    private lateinit var data : UsersDataSource
    private lateinit var filesDir : MutableLiveData<File>
    fun setFilesDir(filesDirectory : File){
        filesDir = MutableLiveData(filesDirectory)
        data = UsersDataSource(filesDir.value!!)
        Log.d("File", filesDir.value.toString())
    }


    fun onEvent(event: RegistrationFormEvent){
        when(event){
            is RegistrationFormEvent.EmailChanged -> {
                _state.value = _state.value.copy(email = event.email)
                submitData()
            }
            is RegistrationFormEvent.UsernameChanged -> {
                _state.value = _state.value.copy(username = event.username)
                submitData()
            }
            is RegistrationFormEvent.PasswordChanged -> {
                _state.value = _state.value.copy(password = event.password)
                submitData()
            }
            is RegistrationFormEvent.RepeatedPasswordChanged -> {
                _state.value = _state.value.copy(repeatedPassword = event.repeatedPassword)
                submitData()
            }
            is RegistrationFormEvent.Submit -> {
                submitData()
            }

        }
    }
    fun signUp(){
        data.signUp(
            email = state.value.email,
            username = state.value.username,
            password = state.value.password
        )
    }

    fun onEvent(event : LoginFormEvent) {
        when(event) {
           is LoginFormEvent.Submit ->{
               _loginState.value = _loginState.value.copy(
                   username = event.username,
                   password = event.password
               )
               checkUser()
           }
            is LoginFormEvent.AuthenticateUsersToken -> {
                _loginState.value = _loginState.value.copy(
                    token = event.token
                )
                authUsersToken()
            }
            is LoginFormEvent.LoggedOut -> {
                _loginState.value = LoginFormState()
                _token.value = ""
            }



        }


    }

    private fun checkUser() {
        val password = loginState.value.password
        val username = loginState.value.username
        val token = GetToken().sha256(username+password+SALT)
        val userResult = ValidateUser().execute(
            users = data.getUsers(),
            username = loginState.value.username,
            token = token
        )
        val isNotUserValid = !userResult.successful
        if (isNotUserValid){
            _loginState.value = _loginState.value.copy(
                loginErrorMessage = userResult.errorMessage,
                isUserValid = false
            )
            return
        }

        _loginState.value = _loginState.value.copy(
            token = token,
            loginErrorMessage = userResult.errorMessage,
            isUserValid = true
        )
        _token.value = token
        return

    }


    fun authUsersToken(){
        val token = loginState.value.token
        val (email : String, username : String) = data.getUserData(token)
        if (email != "" && username != "") {
            _loginState.value = _loginState.value.copy(
                username = username,
                email = email,
                isUserAuthenticated = true
            )
            return
        }
        _loginState.value = _loginState.value.copy(
            isUserAuthenticated = false
        )

    }

    private fun submitData() {
        val emailResult = ValidateEmail().execute(state.value.email)
        val usernameResult = ValidateUsername().execute(state.value.username)
        val passwordResult = ValidatePassword().execute(state.value.password)
        val repeatedPasswordResult = ValidateRepeatedPassword().execute(
            state.value.password,
            state.value.repeatedPassword)

        val hasError = listOf(
            emailResult,
            usernameResult,
            passwordResult,
            repeatedPasswordResult
        ).any{!it.successful}

        if (hasError){
            _state.value = _state.value.copy(
                emailError = emailResult.errorMessage,
                usernameError = usernameResult.errorMessage,
                passwordError = passwordResult.errorMessage,
                repeatedPasswordError = repeatedPasswordResult.errorMessage
            )
            return
        }
        // If there are no errors set all ErrorMessages in State to null
        _state.value = _state.value.copy(
            emailError = emailResult.errorMessage,
            usernameError = usernameResult.errorMessage,
            passwordError = passwordResult.errorMessage,
            repeatedPasswordError = repeatedPasswordResult.errorMessage,
            isAllDataValid = true
        )


    }


}