package com.example.authwebapp.adapter

import android.app.PendingIntent.getActivity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.example.authwebapp.R
import com.example.authwebapp.SharedWebViewModel
import com.example.authwebapp.data.Datasource.links

class LinkAdapter(private val viewModel : SharedWebViewModel) : RecyclerView.Adapter<LinkAdapter.LinkViewHolder>() {
    val dataset = links
    class LinkViewHolder(val view : View) : RecyclerView.ViewHolder(view){
        val logoImage : ImageView = view.findViewById(R.id.logo)
        val nameText : TextView = view.findViewById(R.id.resource_name)
        val button : Button = view.findViewById(R.id.open_button)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) : LinkViewHolder{
        val adapterLayout = LayoutInflater.from(parent.context).inflate(R.layout.link_item_card, parent, false)
        return LinkViewHolder(adapterLayout)
    }
    override fun onBindViewHolder(holder : LinkViewHolder, position: Int){

        holder.logoImage.setImageResource(dataset[position].logoId)
        holder.nameText.text=dataset[position].resourceName
        holder.button.setOnClickListener{
            val link = dataset[position].urlLink
            viewModel.setLink(link)
            Navigation.findNavController(holder.view).navigate(R.id.webFragment)
        }
    }
    override fun getItemCount() : Int{
        return dataset.size
    }

}