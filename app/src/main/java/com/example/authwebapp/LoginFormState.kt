package com.example.authwebapp

data class LoginFormState(
    val username : String = "",
    val password : String = "",
    val email : String = "",
    val token : String = "",
    val loginErrorMessage : String? = null,
    val isUserValid : Boolean = false,
    val isUserAuthenticated : Boolean = false
)