package com.example.authwebapp.data

import android.util.Log
import com.example.authwebapp.MainActivity
import com.example.authwebapp.domain.use_case.GetToken
import com.example.authwebapp.model.User
import com.example.authwebapp.model.Users
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import java.io.*

class UsersDataSource(val filesDir : File) {

    val SALT = "VibeLab"

    val file = File(filesDir, MainActivity.FILE_NAME)
    var usersString = jsonToString(file)

    /**
     * Generate token for new User and add it to other Users in JSON
     */
    fun signUp(
        email: String,
        username: String,
        password: String
    ) {
        val token = GetToken().sha256(username + password + SALT)
        // Define File path and its name
        val user = User(email, username, token)
        addUser(file, user)
    }

    fun singIn(username: String, password: String) {


        val token = GetToken().sha256(username + password + SALT)
        // Define File path and its name
        val users = getUsers()

    }


    val moshi = Moshi.Builder().build()
    val jsonAdapter: JsonAdapter<Users> = moshi.adapter(Users::class.java)

    /**
     * Add User to JSON (to users)
     */
    fun addUser(file: File, user: User) {


        Log.d("JSON", usersString.toString())
        val users = getUsers()

        // converting Json File to users

        //add user to list of Users
        users.list.add(user)
        usersString = jsonAdapter.toJson(users)

        Log.d("JSON", usersString.toString())

        addToJson(file, usersString)
    }

    /**
     * Get Users Data using its token
     */
    fun getUserData(token : String) : Pair<String, String>{
        val users = getUsers()

        for (user in users.list){
            if (user.token == token){
               return Pair(user.email, user.username)
            }
        }
        return Pair("", "")
    }

    /**
     * Get All Users from JSON file
     */
    fun getUsers(): Users {
        lateinit var users: Users

        if (usersString == "") {
            users = Users(mutableListOf<User>())
        } else {
            users = jsonAdapter.fromJson(usersString)!!
        }
        return users
    }

    /**
     * Add string(usually User) to JSON
     * Create File if it is not exists
     */

    fun addToJson(file: File, string: String) {
        val fileWriter: FileWriter = FileWriter(file)
        val bufferedWriter: BufferedWriter = BufferedWriter(fileWriter)
        bufferedWriter.write(string)
        bufferedWriter.close()
    }

    /**
     * Convert Json file to String
     */
    fun jsonToString(file: File): String {
        lateinit var fileReader : InputStreamReader

        // If file does not exists create it
        try {
            fileReader = file.reader()
        } catch (e : FileNotFoundException){
            // if file does not exists create it
            addToJson(file, "")
            fileReader = file.reader()
        }

        val bufferedReader = BufferedReader(fileReader)
        val stringBuilder = StringBuilder()
        var line = bufferedReader.readLine()
        while (line != null) {
            stringBuilder.append(line).append("\n")
            line = bufferedReader.readLine()
        }

        return stringBuilder.toString()
    }
}