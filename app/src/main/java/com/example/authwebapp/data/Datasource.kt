package com.example.authwebapp.data

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.example.authwebapp.MainActivity
import com.example.authwebapp.R
import com.example.authwebapp.domain.use_case.GetToken
import com.example.authwebapp.model.Link
import com.example.authwebapp.model.RandomStyle
import com.example.authwebapp.model.User
import com.example.authwebapp.model.Users
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import java.io.BufferedReader
import java.io.BufferedWriter
import java.io.File
import java.io.FileWriter

object Datasource {
        val links : List<Link> = listOf(
            Link(R.drawable.google,"Google", "https://www.google.com/"),
            Link(R.drawable.vk,"VK", "https://vk.com/"),
            Link(R.drawable.vibelab,"VibeLab", "https://vibelab.etu.ru/")
        )
    fun getRandomTheme() : RandomStyle {
      return listOf(
          RandomStyle(
              R.style.Theme_second
          ),
          RandomStyle(
                      R.style.Theme_AuthWebApp
          )
        ).random()

    }



}