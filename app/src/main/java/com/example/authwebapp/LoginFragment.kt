package com.example.authwebapp

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.example.authwebapp.data.Datasource
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import kotlinx.coroutines.flow.collectLatest

class LoginFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {


        val navController = findNavController()
        val signInButton : Button = view.findViewById(R.id.sign_in)
        val signUpButton : Button = view.findViewById(R.id.sign_up)

        val usernameEdit : TextInputEditText = view.findViewById(R.id.username_field)
        val usernameField : TextInputLayout = view.findViewById(R.id.username_layout)

        val passwordEdit : TextInputEditText = view.findViewById(R.id.password_field)
        val passwordField : TextInputLayout = view.findViewById(R.id.password_layout)




        val userViewModel : UserViewModel by activityViewModels()

        // Authenticate user if saved Token in SharedPrefs is valid

        lifecycleScope.launchWhenStarted{
            userViewModel.loginState.collectLatest{ state ->
                if (state.isUserAuthenticated){
                    navController.navigate(R.id.homeFragment)
                    Toast.makeText(context, "Hello, ${state.username}!", Toast.LENGTH_SHORT).show()
                }

            }

        }


        // Check wether users data valid
        // Authorize User if it is so
        // Navigate to homeFragment
        signInButton.setOnClickListener{
            userViewModel.onEvent(LoginFormEvent.Submit(
                username = usernameEdit.text.toString(),
                password = passwordEdit.text.toString()
            ))

            lifecycleScope.launchWhenStarted{
                userViewModel.loginState.collectLatest{ state ->
                    val hasError : Boolean = state.loginErrorMessage != null
                    if (hasError) {
                        usernameField.isErrorEnabled = hasError
                        usernameField.error = state.loginErrorMessage
                        passwordField.isErrorEnabled = hasError
                    }
                    if (state.isUserValid) {
                        Toast.makeText(context, "Hello, ${state.username}!", Toast.LENGTH_SHORT).show()
                        navController.navigate(R.id.homeFragment)
                    }
                }
            }


        }
        // Navigates to RegistrationFragment if Sign Up button was clicked
        signUpButton.setOnClickListener{

            Navigation.findNavController(it).navigate(R.id.action_loginFragment_to_registrationFragment)
        }
        super.onViewCreated(view, savedInstanceState)
    }

}