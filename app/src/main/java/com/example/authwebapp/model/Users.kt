package com.example.authwebapp.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true )
data class Users(@Json(name = "users") val list : MutableList<User>)