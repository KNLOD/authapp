package com.example.authwebapp.model

import androidx.annotation.DrawableRes

class Link(
    @DrawableRes val logoId : Int,
    val resourceName : String,
    val urlLink: String
)
