package com.example.authwebapp.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class User(
    @Json (name = "email")
    val email : String,
    @Json (name = "username")
    val username: String,
    @Json (name = "token")
    val token: String
)
